# Installation

Activate Extension

# Functionality

* Adds several pagetypes and
* export settings to control the export of the pages

# default values for the export with pages

```
# All targets
TCEMAIN.pages.tx_impexphelper_target="-1"

# no target
TCEMAIN.pages.tx_impexphelper_target="-2"

# targets 1,2,3
TCEMAIN.pages.tx_impexphelper_target="1,2,3"
```

# Commandline

Export all the PIDs equal and below a given uid

```bash
vendor/bin/typo3cms impexphelper:tree 2350
```

# Migration from 9.8.2

```sql
INSERT INTO tx_impexphelper_target (uid, header) VALUES (151, 'Kreisverband');
INSERT INTO tx_impexphelper_target (uid, header) VALUES (152, 'Ortsverband');
INSERT INTO tx_impexphelper_target (uid, header) VALUES (153, 'Landesverband');
INSERT INTO tx_impexphelper_target (uid, header) VALUES (154, 'BRK');
INSERT INTO tx_impexphelper_target (uid, header) VALUES (155, 'MH - KV Mittelhessen');
INSERT INTO tx_impexphelper_target (uid, header) VALUES (156, 'KO - KV Offen');
INSERT INTO tx_impexphelper_target (uid, header) VALUES (157, 'OL - LV Oldenburg');
INSERT INTO tx_impexphelper_target (uid, header) VALUES (158, 'Wasserwacht');
INSERT INTO tx_impexphelper_target (uid, header) VALUES (159, 'Jugendrotkreuz');
INSERT INTO tx_impexphelper_target (uid, header) VALUES (160, 'Bergwacht');

UPDATE pages SET tx_impexphelper_target=-1 WHERE tx_impexphelper_target IS NULL;
UPDATE pages SET doktype=1, tx_impexphelper_target=151 WHERE doktype=151;
UPDATE pages SET doktype=1, tx_impexphelper_target=152 WHERE doktype=152;
UPDATE pages SET doktype=1, tx_impexphelper_target=153 WHERE doktype=153;
UPDATE pages SET doktype=1, tx_impexphelper_target=154 WHERE doktype=154;
UPDATE pages SET doktype=1, tx_impexphelper_target=155 WHERE doktype=155;
UPDATE pages SET doktype=1, tx_impexphelper_target=156 WHERE doktype=156;
UPDATE pages SET doktype=1, tx_impexphelper_target=157 WHERE doktype=157;
UPDATE pages SET doktype=1, tx_impexphelper_target=158 WHERE doktype=158;
UPDATE pages SET doktype=1, tx_impexphelper_target=159 WHERE doktype=159;
UPDATE pages SET doktype=1, tx_impexphelper_target=160 WHERE doktype=160;


UPDATE tt_content SET tx_impexphelper_target="-1" WHERE tx_impexphelper_donotshowon IS NULL;
UPDATE tt_content SET tx_impexphelper_target="152,153" WHERE tx_impexphelper_donotshowon=1;
UPDATE tt_content SET tx_impexphelper_target="151,153" WHERE tx_impexphelper_donotshowon=2;
UPDATE tt_content SET tx_impexphelper_target="153" WHERE tx_impexphelper_donotshowon=3;
UPDATE tt_content SET tx_impexphelper_target="151,152" WHERE tx_impexphelper_donotshowon=4;
UPDATE tt_content SET tx_impexphelper_target="152" WHERE tx_impexphelper_donotshowon=5;
UPDATE tt_content SET tx_impexphelper_target="151" WHERE tx_impexphelper_donotshowon=6;
UPDATE tt_content SET tx_impexphelper_target=-2 WHERE tx_impexphelper_donotshowon=7;
```

Verify the Update

The Number of the 2 Queries should match!

```sql
SELECT count(*), tx_impexphelper_donotshowon, tx_impexphelper_target FROM tt_content GROUP BY tx_impexphelper_donotshowon;
SELECT count(*), tx_impexphelper_donotshowon, tx_impexphelper_target FROM tt_content GROUP BY tx_impexphelper_target;
```
