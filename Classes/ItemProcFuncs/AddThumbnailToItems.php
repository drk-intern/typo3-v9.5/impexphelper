<?php
/**
 * Created by kay.
 */

namespace KayStrobach\Impexphelper\ItemProcFuncs;


use KayStrobach\Impexphelper\Slots\TcaManipulationSlot;
use TYPO3\CMS\Core\Database\ConnectionPool;
use TYPO3\CMS\Core\Database\Query\QueryBuilder;
use TYPO3\CMS\Core\Utility\GeneralUtility;

class AddThumbnailToItems
{
    public function getCategoryFieldsForTable(array &$configuration)
    {
        foreach($configuration['items'] as $key => $item) {
            #if (is_string($item[2])) {
            #    continue;
            #}
            try {
                $file = $this->getFileByUid($item[1]);
                if ($file !== null) {
                    $configuration['items'][$key][2] = $file->getPublicUrl();
                }
            } catch (\Exception $e) {
                $configuration['items'][$key][1] .= ' - ' . $e->getMessage();
            }
        }
    }

    protected function getFileByUid($uid)
    {
        $fileRepository = GeneralUtility::makeInstance(\TYPO3\CMS\Core\Resource\FileRepository::class);
        $fileObjects = $fileRepository->findByRelation(TcaManipulationSlot::TABLE_NAME, 'image', $uid);
        if (count($fileObjects) === 0) {
            return null;
        }
        return array_shift($fileObjects);
    }
}
