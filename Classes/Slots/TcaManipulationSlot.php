<?php
namespace KayStrobach\Impexphelper\Slots;

use KayStrobach\Impexphelper\ItemProcFuncs\AddThumbnailToItems;

class TcaManipulationSlot
{
    public const FIELDNAME = 'tx_impexphelper_target';

    public const TABLE_NAME = 'tx_impexphelper_target';

    public const ICON_POSITIVE = 'status-status-permission-granted';

    public const ICON_NEGATIVE = 'status-status-permission-denied';

    public function addHelperFieldsToAllTables(array $tca)
    {
        // iterate overall tables and add to all types

        $GLOBALS['TCA'] = $tca;

        foreach ($tca as $tableName => $tableConf) {
            if ($tableName === self::TABLE_NAME) {
                continue;
            }
            \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addTCAcolumns(
                $tableName,
                [
                    'tx_impexphelper_target' => [
                        'exclude' => 1,
                        'label' => 'LLL:EXT:impexphelper/Resources/Private/Language/locallang_general.xlf:LGL.tx_impexphelper_target',
                        'config' => [
                            'type' => 'select',
                            'renderType' => 'selectCheckBox',
                            'size' => 5,
                            'default' => -1,
                            'minitems' => 1,
                            'maxitems' => 1000,
                            'exclusiveKeys' => '-1,-2',
                            'foreign_table' => self::TABLE_NAME,
                            'foreign_table_where' => 'ORDER BY ' . self::TABLE_NAME . '.header',
                            'enableMultiSelectFilterTextfield' => true,
                            'itemsProcFunc' => AddThumbnailToItems::class . '->getCategoryFieldsForTable',
                            'items' => [
                                [
                                    'LLL:EXT:impexphelper/Resources/Private/Language/locallang_general.xlf:LGL.All',
                                    -1,
                                    self::ICON_POSITIVE
                                ],
                                [
                                    'LLL:EXT:impexphelper/Resources/Private/Language/locallang_general.xlf:LGL.None',
                                    -2,
                                    self::ICON_NEGATIVE
                                ],
                                # [
                                #     'LLL:EXT:impexphelper/Resources/Private/Language/locallang_general.xlf:LGL.target',
                                #     '--div--'
                                # ]
                            ]
                        ]
                    ]
                ]
            );

            \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addToAllTCAtypes(
                $tableName,
                '--div--;LLL:EXT:impexphelper/Resources/Private/Language/locallang_general.xlf:tab,' . self::FIELDNAME
            );
        }

        $tca = $GLOBALS['TCA'];

        return [$tca];
    }
}
