<?php

namespace KayStrobach\Impexphelper\Service;

use KayStrobach\Impexphelper\Slots\TcaManipulationSlot;
use TYPO3\CMS\Core\Resource\FileRepository;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Lang\LanguageService;

class RenderExportInfoService
{
    public function render($row)
    {
        if ($row[TcaManipulationSlot::FIELDNAME] === '' || $row[TcaManipulationSlot::FIELDNAME] === null) {
            return;
        }
        if ((string)$row[TcaManipulationSlot::FIELDNAME] === '-1') {
            return $this->wrapExportHints(
                \TYPO3\CMS\Backend\Form\Utility\FormEngineUtility::getIconHtml(
                    TcaManipulationSlot::ICON_POSITIVE,
                    $this->getLanguageService()->sL('LLL:EXT:impexphelper/Resources/Private/Language/locallang_general.xlf:LGL.All'),
                    $this->getLanguageService()->sL('LLL:EXT:impexphelper/Resources/Private/Language/locallang_general.xlf:LGL.All')
                ) . $this->getLanguageService()->sL('LLL:EXT:impexphelper/Resources/Private/Language/locallang_general.xlf:LGL.All')
            );
        }
        if ((string)$row[TcaManipulationSlot::FIELDNAME] === '-2') {
            return $this->wrapExportHints(
                \TYPO3\CMS\Backend\Form\Utility\FormEngineUtility::getIconHtml(
                    TcaManipulationSlot::ICON_NEGATIVE,
                    $this->getLanguageService()->sL('LLL:EXT:impexphelper/Resources/Private/Language/locallang_general.xlf:LGL.None'),
                    $this->getLanguageService()->sL('LLL:EXT:impexphelper/Resources/Private/Language/locallang_general.xlf:LGL.None')
                ) . $this->getLanguageService()->sL('LLL:EXT:impexphelper/Resources/Private/Language/locallang_general.xlf:LGL.None')
            );
        }
        $items = explode(',', $row[TcaManipulationSlot::FIELDNAME]);
        $buffer = '';
        foreach ($items as $item) {
            $file = $this->getFileByUid($item);
            $exporterRow = \TYPO3\CMS\Backend\Utility\BackendUtility::getRecord(
                TcaManipulationSlot::TABLE_NAME,
                $item,
                'header'
            );
            if ($file !== null) {
                $buffer .=
                    '<span class="t3js-icon icon icon-size-small icon-state-default">'
                    . \TYPO3\CMS\Backend\Form\Utility\FormEngineUtility::getIconHtml(
                        $file->getPublicUrl(),
                        $exporterRow['header'],
                        $exporterRow['header']
                    )
                    . '</span>'
                ;
            } else {
                $buffer .= ' - ' . $exporterRow['header'];
            }

        }
        return $this->wrapExportHints($buffer);
    }

    protected function wrapExportHints(string $content)
    {
        if ($content === '') {
            return '';
        }
        return '<div class="callout callout-info"><div class="media">
            <div class="media-left">
                <span class="fa-stack fa-lg callout-icon"><i class="fa fa-circle fa-stack-2x"></i><i class="fa fa-info fa-stack-1x"></i></span>
            </div>
            <div class="media-body">
            ' . $this->getLanguageService()->sL('LLL:EXT:impexphelper/Resources/Private/Language/locallang_general.xlf:export_for') . ': ' . $content . '
            </div>
        </div></div>';
    }

    /**
     * @return LanguageService
     */
    protected function getLanguageService()
    {
        return $GLOBALS['LANG'];
    }

    protected function getFileByUid($uid)
    {
        $fileRepository = GeneralUtility::makeInstance(FileRepository::class);
        $fileObjects = $fileRepository->findByRelation(
            TcaManipulationSlot::TABLE_NAME,
            'image',
            $uid
        );
        if (count($fileObjects) === 0) {
            return null;
        }
        return array_shift($fileObjects);
    }
}
