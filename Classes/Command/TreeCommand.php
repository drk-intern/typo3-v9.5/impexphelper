<?php
namespace KayStrobach\Impexphelper\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use TYPO3\CMS\Core\Utility\GeneralUtility;

class TreeCommand extends Command
{
    /**
     * Configure the command by defining the name, options and arguments
     */
    protected function configure()
    {
        $this->setDescription('Show tree nodes for a given uid');
        $this->setHelp('');
        $this->addArgument('pid', InputArgument::REQUIRED, 'The starting page uid');
    }

    /**
     * Executes the command for showing sys_log entries
     *
     * @param InputInterface $input
     * @param OutputInterface $output
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $parent = $input->getArgument('pid');
        $depth = 999999;
        $queryGenerator = GeneralUtility::makeInstance( \TYPO3\CMS\Core\Database\QueryGenerator::class);
        $childPids = $queryGenerator->getTreeList($parent, $depth, 0, 1); //Will be a string like 1,2,3
        $output->writeln($childPids);
        return 0;
    }
}
